<?php

namespace QBNK\Connectors\TemplafyHive\Enums;

enum LibraryType: string
{
    case Documents = 'documents';
    case Presentations = 'presentations';
    case Spreadsheets = 'spreadsheets';
    case Slides = 'slides';
    case SlideElements = 'slide-elements';
    case TextElements = 'text-elements';
    case Images = 'images';
    case Pdfs = 'pdfs';
    case  Links = 'links';
    case EmailElements = 'email-elements';
}