<?php

namespace QBNK\Connectors\TemplafyHive;

use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use QBNK\Connectors\TemplafyHive\Resources\FolderResource;
use QBNK\Connectors\TemplafyHive\Resources\ImageResource;
use QBNK\Connectors\TemplafyHive\Resources\LibraryResource;
use Saloon\Http\Auth\TokenAuthenticator;
use Saloon\Http\Connector;
use Saloon\Http\Response;
use Saloon\RateLimitPlugin\Contracts\RateLimitStore;
use Saloon\RateLimitPlugin\Limit;
use Saloon\RateLimitPlugin\Stores\MemoryStore;
use Saloon\RateLimitPlugin\Traits\HasRateLimits;
use Saloon\Traits\Plugins\AcceptsJson;
use Saloon\Traits\Plugins\AlwaysThrowOnErrors;

class TemplafyHiveSdk extends Connector
{
    use AlwaysThrowOnErrors;
    use AcceptsJson;
    use HasRateLimits;

    public function __construct(protected readonly string $tenant, string $bearerToken)
    {
        $this->authenticate(new TokenAuthenticator($bearerToken));
    }

    public function getRequestException(Response $response, ?\Throwable $senderException): ?\Throwable
    {
        // Do we really need to throw custom exceptions?
        return match ($response->status()) {
            default => $senderException
        };
    }

    public function resolveBaseUrl(): string
    {
        return sprintf('https://%s.api.templafy.com/v2', $this->tenant);
    }

    public function folders(int $spaceId, LibraryType $libraryType): FolderResource
    {
        return new FolderResource($this, $spaceId, $libraryType);
    }

    public function libraries(): LibraryResource
    {
        return new LibraryResource($this);
    }

    public function images(int $spaceId): ImageResource
    {
        return new ImageResource($this, $spaceId);
    }

    protected function resolveLimits(): array
    {
        return [
            Limit::allow(500, threshold: 0.8)->everySeconds(10)->sleep(),
            Limit::allow(30000, threshold: 0.9)->everyHour()->sleep(),
        ];
    }

    protected function resolveRateLimitStore(): RateLimitStore
    {
        return new MemoryStore();
    }
}