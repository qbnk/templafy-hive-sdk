<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Libraries;

use QBNK\Connectors\TemplafyHive\Data\Library;
use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use Saloon\Http\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Traits\Body\HasJsonBody;
use Saloon\Traits\Plugins\AcceptsJson;

class GetLibraryByType extends Request
{
    protected Method $method = Method::GET;

    public function __construct(protected readonly int $spaceId, protected readonly LibraryType $libraryType)
    {
    }

    public function resolveEndpoint(): string
    {
        return sprintf('/libraries/%s/%s', $this->spaceId, $this->libraryType->value);
    }

    public function createDtoFromResponse(Response $response): mixed
    {
        return Library::fromResponse($response);
    }
}