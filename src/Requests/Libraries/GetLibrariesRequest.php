<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Libraries;

use QBNK\Connectors\TemplafyHive\Data\Collections\LibraryCollection;
use Saloon\Http\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;

class GetLibrariesRequest extends Request
{
    protected Method $method = Method::GET;

    public function resolveEndpoint(): string
    {
        return '/libraries';
    }

    public function createDtoFromResponse(Response $response): mixed
    {
        return LibraryCollection::fromResponse($response);
    }
}