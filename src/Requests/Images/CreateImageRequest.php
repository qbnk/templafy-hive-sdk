<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Images;

use GuzzleHttp\RequestOptions;
use Saloon\Contracts\Body\HasBody;
use Saloon\Http\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Traits\Body\HasMultipartBody;

class CreateImageRequest extends Request implements HasBody
{
    use HasMultipartBody;

    protected Method $method = Method::POST;

    public function __construct(
        protected readonly int $spaceId,
        protected readonly int $folderId,
        protected readonly \SplFileInfo $file,
        protected readonly ?string $name = null,
        protected readonly ?string $description = null,
        protected readonly array $tags = [],
        protected readonly string $externalData = '',
    ) {
    }

    public function resolveEndpoint(): string
    {
        return sprintf('/libraries/%d/images/folders/%d/assets', $this->spaceId, $this->folderId);
    }

    protected function defaultConfig(): array
    {
        $multipart = [
            [
                'name' => 'Name',
                'contents' => $this->name,
            ],
            [
                'name' => 'Description',
                'contents' => $this->description,
            ],
            [
                'name' => 'ExternalData',
                'contents' => $this->externalData,
            ],
            [
                'name' => 'File',
                'contents' => fopen($this->file->getPathname(), 'r'),
                'filename' => $this->file->getFilename(),
            ],
        ];

        foreach ($this->tags as $tag) {
            $multipart[] = [
                'name' => 'Tags',
                'contents' => $tag,
            ];
        }

        return [
            RequestOptions::MULTIPART => $multipart
        ];
    }

    public function createDtoFromResponse(Response $response): mixed
    {
        return intval(json_decode($response->body()));
    }
}