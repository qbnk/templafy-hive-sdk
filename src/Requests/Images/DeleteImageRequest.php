<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Images;

use GuzzleHttp\RequestOptions;
use Saloon\Contracts\Body\HasBody;
use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Traits\Body\HasMultipartBody;

class DeleteImageRequest extends Request
{
    protected Method $method = Method::DELETE;

    public function __construct(
        protected readonly int $spaceId,
        protected readonly int $assetId,
    ) {
    }

    public function resolveEndpoint(): string
    {
        return sprintf('/libraries/%d/images/assets/%d', $this->spaceId, $this->assetId);
    }
}