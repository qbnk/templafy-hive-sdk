<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Images;

use QBNK\Connectors\TemplafyHive\Data\Image;
use Saloon\Http\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;

class GetImageByIdRequest extends Request
{
    protected Method $method = Method::GET;

    public function __construct(
        protected readonly int $spaceId,
        protected readonly int $id
    ) {
    }

    public function resolveEndpoint(): string
    {
        return sprintf('/libraries/%s/images/assets/%d', $this->spaceId, $this->id);
    }

    public function createDtoFromResponse(Response $response): mixed
    {
        return Image::fromResponse($response);
    }
}