<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Images;

use GuzzleHttp\RequestOptions;
use Saloon\Contracts\Body\HasBody;
use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Traits\Body\HasMultipartBody;

class UpdateImageRequest extends Request implements HasBody
{
    use HasMultipartBody;

    protected Method $method = Method::PATCH;

    public function __construct(
        protected readonly int $spaceId,
        protected readonly ?int $assetId,
        protected readonly ?int $folderId = null,
        protected readonly ?\SplFileInfo $file = null,
        protected readonly ?string $name = null,
        protected readonly ?string $description = null,
        protected readonly array $tags = [],
        protected readonly ?string $externalData = null,
    ) {
    }

    public function resolveEndpoint(): string
    {
        return sprintf('/libraries/%d/images/assets/%d', $this->spaceId, $this->assetId);
    }

    protected function defaultConfig(): array
    {
        $data = [
            'FolderId' => $this->folderId,
            'Name' => $this->name,
            'Description' => $this->description,
            'ExternalData' => $this->externalData
        ];

        return [
            RequestOptions::MULTIPART => collect($data)
                ->map(fn($value, $key) => ['name' => $key, 'contents' => $value])
                ->merge(collect($this->tags)->map(fn($tag) => ['name' => 'Tags', 'contents' => $tag]))
                ->add([
                    'name' => 'File',
                    'contents' => $this->file ? fopen($this->file->getPathname(), 'r') : null,
                    'filename' => $this->file ? $this->file->getFilename() : ''
                ])
                ->filter(fn($value) => $value['contents'] !== null)
                ->toArray()
        ];
    }
}