<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Images;

use QBNK\Connectors\TemplafyHive\Data\Collections\ImageCollection;
use Saloon\Http\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;

class GetImagesRequest extends Request
{
    protected Method $method = Method::GET;

    public function __construct(
        protected readonly int $spaceId,
        protected readonly int $folderId,
        protected readonly string $searchQuery = '',
        protected readonly int $pageNumber = 1,
        protected readonly int $pageSize = 100
    ) {
    }

    public function resolveEndpoint(): string
    {
        return sprintf('/libraries/%d/images/folders/%d/assets', $this->spaceId, $this->folderId);
    }

    protected function defaultQuery(): array
    {
        return [
            'searchQuery' => $this->searchQuery,
            'pageNumber' => $this->pageNumber,
            'pageSize' => $this->pageSize,
        ];
    }

    public function createDtoFromResponse(Response $response): mixed
    {
        return ImageCollection::fromResponse($response);
    }
}