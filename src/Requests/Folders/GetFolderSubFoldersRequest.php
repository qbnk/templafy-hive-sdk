<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Folders;

use QBNK\Connectors\TemplafyHive\Data\Collections\FolderCollection;
use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use Saloon\Http\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;

class GetFolderSubFoldersRequest extends Request
{
    protected Method $method = Method::GET;

    public function __construct(protected int $spaceId, protected LibraryType $libraryType, protected int $id)
    {
    }

    public function resolveEndpoint(): string
    {
        return sprintf('/libraries/%s/%s/folders/%d/folders', $this->spaceId, $this->libraryType->value, $this->id);
    }

    public function createDtoFromResponse(Response $response): mixed
    {
        return FolderCollection::fromResponse($response);
    }
}