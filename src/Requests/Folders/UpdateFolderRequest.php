<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Folders;

use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use Saloon\Contracts\Body\HasBody;
use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Traits\Body\HasJsonBody;

class UpdateFolderRequest extends Request implements HasBody
{
    use HasJsonBody;

    protected Method $method = Method::PATCH;

    public function __construct(
        protected readonly int $spaceId,
        protected readonly LibraryType $libraryType,
        protected readonly int $id,
        protected readonly string $name,
        protected readonly ?int $newParentId = null,
    ) {
    }

    public function resolveEndpoint(): string
    {
        return sprintf(
            '/libraries/%s/%s/folders/%d',
            $this->spaceId,
            $this->libraryType->value,
            $this->id
        );
    }

    protected function defaultBody(): array
    {
        return [
            'name' => $this->name,
            'parentFolderId' => $this->newParentId
        ];
    }
}