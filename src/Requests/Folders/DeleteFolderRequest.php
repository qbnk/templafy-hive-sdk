<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Folders;

use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use Saloon\Contracts\Body\HasBody;
use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Traits\Body\HasJsonBody;

class DeleteFolderRequest extends Request implements HasBody
{
    use HasJsonBody;

    protected Method $method = Method::DELETE;

    public function __construct(
        protected readonly int $spaceId,
        protected readonly LibraryType $libraryType,
        protected readonly int $id,
    ) {
    }

    public function resolveEndpoint(): string
    {
        return sprintf(
            '/libraries/%s/%s/folders/%d',
            $this->spaceId,
            $this->libraryType->value,
            $this->id
        );
    }
}