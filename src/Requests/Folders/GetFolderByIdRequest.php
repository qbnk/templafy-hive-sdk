<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Folders;

use QBNK\Connectors\TemplafyHive\Data\Folder;
use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use Saloon\Http\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;

class GetFolderByIdRequest extends Request
{
    protected Method $method = Method::GET;

    public function __construct(
        protected readonly int $spaceId,
        protected readonly LibraryType $libraryType,
        protected readonly int $id
    ) {
    }

    public function resolveEndpoint(): string
    {
        return sprintf('/libraries/%s/%s/folders/%d', $this->spaceId, $this->libraryType->value, $this->id);
    }

    public function createDtoFromResponse(Response $response): mixed
    {
        return Folder::fromResponse($response);
    }
}