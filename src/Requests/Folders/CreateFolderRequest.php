<?php

namespace QBNK\Connectors\TemplafyHive\Requests\Folders;

use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use Saloon\Contracts\Body\HasBody;
use Saloon\Http\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Traits\Body\HasJsonBody;

class CreateFolderRequest extends Request implements HasBody
{
    use HasJsonBody;

    protected Method $method = Method::POST;

    public function __construct(
        protected readonly int $spaceId,
        protected readonly LibraryType $libraryType,
        protected readonly int $parentId,
        protected readonly string $name
    ) {
    }

    public function resolveEndpoint(): string
    {
        return sprintf(
            '/libraries/%s/%s/folders/%d/folders',
            $this->spaceId,
            $this->libraryType->value,
            $this->parentId
        );
    }

    protected function defaultBody(): array
    {
        return [
            'name' => $this->name,
        ];
    }

    public function createDtoFromResponse(Response $response): mixed
    {
        return intval(json_decode($response->body()));
    }
}