<?php

namespace QBNK\Connectors\TemplafyHive\Resources;

use Illuminate\Support\Collection;
use QBNK\Connectors\TemplafyHive\Data\Folder;
use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use QBNK\Connectors\TemplafyHive\Requests\Folders\CreateFolderRequest;
use QBNK\Connectors\TemplafyHive\Requests\Folders\DeleteFolderRequest;
use QBNK\Connectors\TemplafyHive\Requests\Folders\GetFolderByIdRequest;
use QBNK\Connectors\TemplafyHive\Requests\Folders\GetFolderSubFoldersRequest;
use QBNK\Connectors\TemplafyHive\Requests\Folders\UpdateFolderRequest;
use Saloon\Http\Connector;

class FolderResource extends Resource
{
    public function __construct(
        Connector $connector,
        int $spaceId,
        protected readonly LibraryType $libraryType = LibraryType::Images
    ) {
        parent::__construct($connector, $spaceId);
    }

    public function get(int $folderId): Folder
    {
        return $this->connector->send(
            new GetFolderByIdRequest($this->spaceId, $this->libraryType, $folderId)
        )->dtoOrFail();
    }

    /**
     * @param int $folderId
     * @return Collection<array-key, Folder>
     */
    public function subfolders(int $folderId): Collection
    {
        return $this->connector->send(
            new GetFolderSubFoldersRequest($this->spaceId, $this->libraryType, $folderId)
        )->dtoOrFail();
    }

    public function create(int $parentId, string $name): int
    {
        return $this->connector->send(
            new CreateFolderRequest($this->spaceId, $this->libraryType, $parentId, $name)
        )->dtoOrFail();
    }

    public function update(int $folderId, string $name, ?int $newParentId = null): void
    {
        $this->connector->send(
            new UpdateFolderRequest($this->spaceId, $this->libraryType, $folderId, $name, $newParentId)
        );
    }

    public function delete(int $folderId): void
    {
        $this->connector->send(
            new DeleteFolderRequest($this->spaceId, $this->libraryType, $folderId)
        );
    }
}