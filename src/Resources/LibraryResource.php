<?php

namespace QBNK\Connectors\TemplafyHive\Resources;

use Illuminate\Support\Collection;
use QBNK\Connectors\TemplafyHive\Data\Library;
use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use QBNK\Connectors\TemplafyHive\Requests\Libraries\GetLibrariesRequest;
use QBNK\Connectors\TemplafyHive\Requests\Libraries\GetLibraryByType;

class LibraryResource extends Resource
{
    /**
     * @return Collection<array-key, Library>
     */
    public function list(): Collection
    {
        return $this->connector->send(
            new GetLibrariesRequest()
        )->dtoOrFail();
    }

    public function getByType(int $spaceId, LibraryType $libraryType): Library
    {
        return $this->connector->send(
            new GetLibraryByType($spaceId, $libraryType)
        )->dtoOrFail();
    }
}