<?php

namespace QBNK\Connectors\TemplafyHive\Resources;

use Saloon\Http\Connector;

class Resource
{
    public function __construct(protected readonly Connector $connector, protected readonly ?int $spaceId = null)
    {
    }
}