<?php

namespace QBNK\Connectors\TemplafyHive\Resources;

use Illuminate\Support\Collection;
use QBNK\Connectors\TemplafyHive\Data\Image;
use QBNK\Connectors\TemplafyHive\Requests\Images\CreateImageRequest;
use QBNK\Connectors\TemplafyHive\Requests\Images\DeleteImageRequest;
use QBNK\Connectors\TemplafyHive\Requests\Images\GetImageByIdRequest;
use QBNK\Connectors\TemplafyHive\Requests\Images\GetImagesRequest;
use QBNK\Connectors\TemplafyHive\Requests\Images\UpdateImageRequest;
use Saloon\Http\Connector;

class ImageResource extends Resource
{
    public function __construct(Connector $connector, int $spaceId)
    {
        parent::__construct($connector, $spaceId);
    }

    /**
     * @param int $folderId
     * @return Collection<array-key, Image>
     */
    public function list(int $folderId, string $searchQuery = '', int $pageNumber = 1, int $pageSize = 100): Collection
    {
        return $this->connector->send(
            new GetImagesRequest($this->spaceId, $folderId, $searchQuery, $pageNumber, $pageSize)
        )->dtoOrFail();
    }

    public function getById(int $imageId): Image
    {
        return $this->connector->send(
            new GetImageByIdRequest($this->spaceId, $imageId)
        )->dtoOrFail();
    }

    public function create(
        int $folderId,
        \SplFileInfo $file,
        string $name = null,
        string $description = null,
        array $tags = [],
        string $externalData = ''
    ): int {
        return $this->connector->send(
            new CreateImageRequest($this->spaceId, $folderId, $file, $name, $description, $tags, $externalData)
        )->dtoOrFail();
    }

    public function update(
        int $assetId,
        ?int $folderId = null,
        ?\SplFileInfo $file = null,
        ?string $name = null,
        ?string $description = null,
        array $tags = [],
        ?string $externalData = null
    ): void {
        $this->connector->send(
            new UpdateImageRequest($this->spaceId, $assetId, $folderId, $file, $name, $description, $tags, $externalData)
        );
    }

    public function delete(int $assetId): void
    {
        $this->connector->send(
            new DeleteImageRequest($this->spaceId, $assetId)
        );
    }
}