<?php

namespace QBNK\Connectors\TemplafyHive\Data\Collections;

use Illuminate\Support\Collection;
use QBNK\Connectors\TemplafyHive\Data\Folder;
use QBNK\Connectors\TemplafyHive\Data\Library;
use Saloon\Contracts\DataObjects\WithResponse;
use Saloon\Http\Response;
use Saloon\Traits\Responses\HasResponse;

/**
 * @template TKey of array-key
 * @template TValue of Folder
 */
class LibraryCollection extends Collection implements WithResponse
{
    use HasResponse;

    public static function fromResponse(Response $response): self
    {
        $data = $response->json();

        return static::make($data)->map(
            fn(array $item): Library => Library::fromArray($item)
        );
    }
}