<?php

namespace QBNK\Connectors\TemplafyHive\Data;

use Saloon\Contracts\DataObjects\WithResponse;
use Saloon\Http\Response;
use Saloon\Traits\Responses\HasResponse;

final class Image implements WithResponse
{
    use HasResponse;

    public function __construct(
        public readonly int $id,
        public readonly int $folderId,
        public readonly string $name,
        public readonly string $description,
        public readonly array $tags,
        public readonly int $fileSize,
        public readonly string $checksum,
        public readonly string $fileExtension,
        public readonly ?string $smallPreviewLink,
        public readonly ?string $largePreviewLink,
        public readonly ?Dimensions $dimensions,
        public readonly string $mimeType,
        public readonly array $automaticTags,
        public readonly string $navigationPath,
        public readonly string $externalData,
        public readonly \DateTimeInterface $modifiedAt
    ) {
    }

    public static function fromResponse(Response $response): self
    {
        return self::fromArray($response->json());
    }

    public static function fromArray(array $data): self
    {
        return new Image(
            $data['id'],
            $data['folderId'],
            $data['name'],
            $data['description'],
            $data['tags'],
            $data['fileSize'],
            $data['checksum'],
            $data['fileExtension'],
            $data['smallPreviewLink'],
            $data['largePreviewLink'],
            isset($data['dimensions']) ? Dimensions::fromArray($data['dimensions']) : null,
            $data['mimeType'],
            $data['automaticTags'],
            $data['navigationPath'],
            $data['externalData'],
            new \DateTimeImmutable($data['modifiedAt'])
        );
    }
}