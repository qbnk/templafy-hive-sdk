<?php

namespace QBNK\Connectors\TemplafyHive\Data;

use QBNK\Connectors\TemplafyHive\Enums\LibraryType;
use Saloon\Contracts\DataObjects\WithResponse;
use Saloon\Http\Response;
use Saloon\Traits\Responses\HasResponse;

final class Library implements WithResponse
{
    use HasResponse;

    public function __construct(
        public readonly int $id,
        public readonly string $name,
        public readonly LibraryType $libraryType,
        public readonly int $spaceId,
        public readonly int $rootFolderId
    ) {
    }

    public static function fromResponse(Response $response): self
    {
        return self::fromArray($response->json());
    }

    public static function fromArray(array $data): self
    {
        return new Library(
            id: $data['id'],
            name: $data['name'],
            libraryType: LibraryType::tryFrom($data['libraryType']),
            spaceId: $data['spaceId'],
            rootFolderId: $data['rootFolderId'],
        );
    }
}
