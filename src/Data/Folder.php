<?php

namespace QBNK\Connectors\TemplafyHive\Data;

use Saloon\Contracts\DataObjects\WithResponse;
use Saloon\Http\Response;
use Saloon\Traits\Responses\HasResponse;

final class Folder implements WithResponse
{
    use HasResponse;

    public function __construct(
        public readonly int $id,
        public readonly int $libraryId,
        public readonly string $name,
        public readonly ?int $parentId,
        public readonly string $navigationPath,
        public readonly \DateTimeInterface $modifiedAt
    ) {
    }

    public static function fromResponse(Response $response): self
    {
        return self::fromArray($response->json());
    }

    public static function fromArray(array $data): self
    {
        return new Folder(
            id: $data['id'],
            libraryId: $data['libraryId'],
            name: $data['name'],
            parentId: $data['parentId'] ?? null,
            navigationPath: $data['navigationPath'],
            modifiedAt: new \DateTimeImmutable($data['modifiedAt']),
        );
    }
}