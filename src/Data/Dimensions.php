<?php

namespace QBNK\Connectors\TemplafyHive\Data;

final class Dimensions
{
    public function __construct(
        public readonly int $height,
        public readonly int $width,
        public readonly ?string $aspectRatio,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new Dimensions(
            $data['height'],
            $data['width'],
            $data['aspectRatio'] ?? null,
        );
    }
}